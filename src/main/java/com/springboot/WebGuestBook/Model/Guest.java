package com.springboot.WebGuestBook.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.io.Serializable;
import javax.persistence.Column;


@Entity
@Table(name="guest_list")
public class Guest implements Serializable {

	
	
	@Id
	@GeneratedValue
	@Column(name="guest_id")
	private Long Id;
	
	public Long getId() {
		return Id;
	}

	@Column(name="guest_name")
	private String guestName;
	
	@Column(name="guest_phone")
	private String guestPhone;
	
	@Column(name="guest_email")
	private String guestEmail;
	
	@Column(name="guest_address")
	private String guestAddress;
	
	@Column(name="guest_message")
	private String guestMessage;
	
	
	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public String getGuestPhone() {
		return guestPhone;
	}

	public void setGuestPhone(String guestPhone) {
		this.guestPhone = guestPhone;
	}

	public String getGuestEmail() {
		return guestEmail;
	}

	public void setGuestEmail(String guestEmail) {
		this.guestEmail = guestEmail;
	}

	public String getGuestAddress() {
		return guestAddress;
	}

	public void setGuestAddress(String guestAddress) {
		this.guestAddress = guestAddress;
	}

	public String getGuestMessage() {
		return guestMessage;
	}

	public void setGuestMessage(String guestMessage) {
		this.guestMessage = guestMessage;
	}
	
}//=================================================================//

