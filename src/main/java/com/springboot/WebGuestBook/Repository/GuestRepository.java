package com.springboot.WebGuestBook.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springboot.WebGuestBook.Model.Guest;

public interface GuestRepository extends JpaRepository<Guest, Long> {
	

List<Guest> findByGuestNameLike (String paramSearch);
	
}
