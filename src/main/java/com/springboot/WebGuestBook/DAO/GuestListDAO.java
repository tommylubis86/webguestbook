package com.springboot.WebGuestBook.DAO;

import com.springboot.WebGuestBook.Model.Guest;
import com.springboot.WebGuestBook.Service.GuestListService;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GuestListDAO implements GuestListService {
	
	private EntityManagerFactory emf;
	
	
	@Autowired 
	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}
	
	@Override
	public List<Guest> listGuest() {
         EntityManager em = emf.createEntityManager();
         return em.createQuery("from Guest",Guest.class)
        		 .getResultList();

	}
	
	@Override
	public Guest saveOrUpdate(Guest inputGuest) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		Guest savedGuest = em.merge(inputGuest);
		
		em.getTransaction().commit();
		
		return savedGuest;
	}
	
	
	
	
}
