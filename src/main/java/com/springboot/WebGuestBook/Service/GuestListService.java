package com.springboot.WebGuestBook.Service;

import java.util.List;
import com.springboot.WebGuestBook.Model.Guest;

public interface GuestListService {
 
 public List<Guest> listGuest(); 
 
 public Guest saveOrUpdate(Guest inputGuest);
 
  
}
