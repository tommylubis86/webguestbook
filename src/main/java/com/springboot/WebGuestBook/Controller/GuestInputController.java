package com.springboot.WebGuestBook.Controller;

import com.springboot.WebGuestBook.Service.GuestListService;

import com.springboot.WebGuestBook.Model.Guest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.ui.Model;

import com.springboot.WebGuestBook.Model.Guest;

import com.springboot.WebGuestBook.Repository.GuestRepository;

import org.springframework.util.MultiValueMap;

@Controller
public class GuestInputController {

    private GuestListService guestListService;
	
	@Autowired
    public void setGuestListService(GuestListService guestListService) {
		this.guestListService = guestListService;
	}

	
	@Autowired 
	private GuestRepository guestRepository;
	  

	@RequestMapping(value = "/guest/create",method = RequestMethod.POST)
    public String saveGuestInput(Model model,Guest inputGuest) {
		Guest newGuest = new Guest();
		
		newGuest.setGuestName(inputGuest.getGuestName());
		newGuest.setGuestPhone(inputGuest.getGuestPhone());
		newGuest.setGuestEmail(inputGuest.getGuestEmail());
		newGuest.setGuestAddress(inputGuest.getGuestAddress());
		newGuest.setGuestMessage(inputGuest.getGuestMessage());
		
    	model.addAttribute("Guest",guestRepository.save(newGuest));
    	return "redirect:/guest-list";
    }
	

	
	@RequestMapping(value = "/guest-book-input",method = RequestMethod.GET)
	public String guestInputBook(Model model)  {
		model.addAttribute("Guest",new Guest());
		return "GuestInputMessage";
	}
	
	
	@RequestMapping(value = "/guest/search",
					method = RequestMethod.POST,
					consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ModelAndView guestSearch(@RequestParam MultiValueMap<String, String> formParameters) 
	{	
		ModelAndView mav = new ModelAndView("search-guest");
				
		 System.out.println("form params received " + formParameters);
		 
		 String searchParam = formParameters.getFirst("guestName");
		 
		 String searchName = "%" + searchParam + "%";
		  
		 List<Guest> searchResult = guestRepository.findByGuestNameLike(searchName);
		  
		 
		 mav.addObject("searchGuestName",searchParam);
		 mav.addObject("GuestList",searchResult);
		    
		 return mav;
	}
	
	
	
}
