package com.springboot.WebGuestBook.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class HomeController {

	@RequestMapping("/")
	public String index_page() {
		return "redirect:/guest-book-input";
	}
}
