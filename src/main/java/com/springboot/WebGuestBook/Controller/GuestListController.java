package com.springboot.WebGuestBook.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.ui.Model;

import com.springboot.WebGuestBook.Service.GuestListService;
import com.springboot.WebGuestBook.Model.Guest;
import com.springboot.WebGuestBook.Repository.GuestRepository;

@Controller
public class GuestListController {
	
	@Autowired 
	private GuestRepository guestRepository;

	@RequestMapping("/guest-list")
	@ModelAttribute("GuestList")
	public List<Guest> guestList(Model model) 
	{	
		List<Guest> listGuest = guestRepository.findAll();
		
		return listGuest;
	}
	
	
	
	

}
