package com.springboot.WebGuestBook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebGuestBookApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebGuestBookApplication.class, args);
	}

}
